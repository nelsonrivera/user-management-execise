package com.example.nelson.usermanagement

import com.example.nelson.usermanagement.controller.UserController
import com.example.nelson.usermanagement.domain.User
import com.example.nelson.usermanagement.repositories.UserRepository
import com.example.nelson.usermanagement.service.UserService
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.containsString
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.TestPropertySource
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.mockito.Mockito
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.test.context.ContextConfiguration
import java.util.*


@ExtendWith(SpringExtension::class)
@WebMvcTest(value = [UserController::class])
@AutoConfigureMockMvc(addFilters = false)
@TestPropertySource(properties = ["security.enabled=false"])
@ContextConfiguration(classes = [ControllerTest.ObjectConfig::class])
class ControllerTest(@Autowired val mockMvc: MockMvc, @Autowired val objectMapper: ObjectMapper) {

    @TestConfiguration
    class ObjectConfig {

        @Bean
        fun objectMapper(): ObjectMapper {
            val objectMapper = ObjectMapper()
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT)
            return objectMapper
        }
    }


    @MockBean
    private lateinit var userRepository: UserRepository


    @MockBean
    private lateinit var userService: UserService

    val USERS_ENDPOINT = "/users"

    @Test
    fun checkObjectMapper() {
        val user = User(uuid = UUID.randomUUID().toString(), name = "John", email = "user@server.cl", password = "Af2g2f", null)

        Assertions.assertTrue(objectMapper.writeValueAsString(user)!!.isNotEmpty())
    }

    @Test
    fun shouldReturnBadRequestWithInvalidField() {
        val jsonPayload = """{
    "name": "juan",
    "email": "juan@server.cl",
    "password": "Auy2"}"""
        mockMvc.perform(post(USERS_ENDPOINT).content(jsonPayload)
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest)
                .andExpect(jsonPath("$.message", `containsString`("Password must have the format one capital letter, lowercase letters and two numbers")))
    }

    @Test
    fun successCreate() {
        val jsonPayload = "{\"uuid\":\"879c3528-3b46-4cfb-94e0-b7edc029808d\",\"name\":\"juan\",\"email\":\"juan@server.cl\",\"password\":\"Auy22\"}"
        val user = User(uuid = UUID.randomUUID().toString(), name = "John", email = "user@server.cl", password = "Af2g2f", null)
        Mockito.`when`(userService.registerUser(any())).thenReturn(user)
        mockMvc.perform(post(USERS_ENDPOINT).content(jsonPayload)
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated).andExpect(jsonPath("$.name", `is`("John")))
    }


     //Esto es un workaround. Porque hay un problema con kotlin y mokito. Vuando vamos a mockear un metodo de un objeto que recibe un objeto non-nullable y queremos usar Mockito.any. Como en el test anterior
     //Ya que todos los Mockito.any devuelven null, esto esta en internet como un issue.
     //Lo que se recomienda es usar una biblioteca que es un wrapper de mockito para kotlin que usa una sintaxis mas kotlin: testImplementation 'com.nhaarman.mockitokotlin2:mockito-kotlin, esta al final del articulo /Documents/DOCUMENTACION/KOTLIN/Kotlin%20with%20Mockito%20%7C%20Baeldung%20on%20Kotlin.html
     //Pero este es un workaround que encontre en este articulo y funciona muy bien : https://medium.com/mobile-app-development-publication/befriending-kotlin-and-mockito-1c2e7b0ef791
    private fun <T> any(): T {
        Mockito.any<T>()
        return uninitialized()
    }    private fun <T> uninitialized(): T = null as T
}
