package com.example.nelson.usermanagement

import com.example.nelson.usermanagement.domain.User
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.util.*
import javax.validation.ConstraintViolation
import javax.validation.Validation
import javax.validation.Validator

class ValidationTest {
    var validator: Validator? = null

    @BeforeEach
    fun setup() {
        val factory = Validation.buildDefaultValidatorFactory()
        validator = factory.validator
    }

    @Test
    fun shouldFailsWithInvalidPassword() {
        val user = User(uuid = UUID.randomUUID().toString(), name = "John", email = "user@server.cl", password = "Af2gf", null)

        val violations = validator?.validate<User>(user)
        Assertions.assertEquals(violations!!.size, 1)
        Assertions.assertEquals("Password must have the format one capital letter, lowercase letters and two numbers", violations.iterator().next().message)
    }

    @Test
    fun shouldBeNoError() {
        val user = User(uuid = UUID.randomUUID().toString(), name = "John", email = "user@server.cl", password = "Af2g2f", null)
        val violations = validator?.validate<User>(user)
        Assertions.assertEquals(violations!!.size, 0)

    }
}
