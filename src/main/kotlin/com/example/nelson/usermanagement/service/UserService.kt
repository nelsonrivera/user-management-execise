package com.example.nelson.usermanagement.service

import com.example.nelson.usermanagement.domain.User
import java.security.Principal

interface UserService {

    fun hello(str: String)
    fun getAllUsers() : List<User>
    fun registerUser(user: User): User
    fun login(principal: Principal): User
}
