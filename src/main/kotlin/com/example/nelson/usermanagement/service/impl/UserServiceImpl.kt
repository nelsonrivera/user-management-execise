package com.example.nelson.usermanagement.service.impl

import com.example.nelson.usermanagement.domain.Phone
import com.example.nelson.usermanagement.domain.User
import com.example.nelson.usermanagement.exception.UniqueException
import com.example.nelson.usermanagement.repositories.UserRepository
import com.example.nelson.usermanagement.security.OAuth2TokenUtils
import com.example.nelson.usermanagement.service.UserService
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.oauth2.common.OAuth2AccessToken
import org.springframework.stereotype.Service
import java.security.Principal
import java.util.*

@Service
class UserServiceImpl(private val repository: UserRepository,
                      private val passwordEncoder: PasswordEncoder,
                      private val jdbcTemplate: JdbcTemplate,
                      private val oAuth2TokenUtils: OAuth2TokenUtils): UserService {

    override fun hello(str: String) {
        println("Hello $str")
    }

    override fun getAllUsers(): List<User> = repository.findAll().toList()
    override fun registerUser(user: User): User {
        checkUniqueEmailAndName(user.email, user.name)

        if(!user.phones.isNullOrEmpty()) {
            user.phones!!.forEach() { phone: Phone -> phone.user = user }
        }

       user.uuid = UUID.randomUUID().toString()
        user.isactive = true
        user.created = Date()
        user.modified = user.created
        user.last_login = user.created
        user.password = passwordEncoder.encode(user.password)
        val save = repository.save(user)

        setAuthoritiesForUser(save)

        return updateToken(save)
    }

    override fun login(principal: Principal): User {
        val user = repository.findFirstByName(principal.name)
        user?.last_login = Date()
        user?.token = oAuth2TokenUtils.getCurrentToken()
        return repository.save(user!!)

    }

    private fun updateToken(user: User): User {
        val oAuth2AccessToken: OAuth2AccessToken = oAuth2TokenUtils.generateAccessToken(user.name)
        user.token = oAuth2AccessToken.value
        return repository.save(user)
    }
    private fun setAuthoritiesForUser(user: User): Int = jdbcTemplate.update("INSERT INTO user_roles (user_name, role) VALUES (?, ?)", user.name, "ROLE_USER")

    private fun checkUniqueEmailAndName(email: String, name: String): Unit {
        val firstByEmail: User? = repository.findFirstByEmail(email)
        if (firstByEmail != null) {
            throw UniqueException("The email $email is already configured.")
        }

        val firstByName: User? = repository.findFirstByName(name)
        if (firstByName != null) {
            throw UniqueException("The name $name is already configured.")
        }
    }
}
