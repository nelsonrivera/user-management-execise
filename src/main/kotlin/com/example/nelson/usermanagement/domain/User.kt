package com.example.nelson.usermanagement.domain

import com.example.nelson.usermanagement.validation.ValidPassword
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotEmpty

@Entity
data class User(
        @Id var uuid: String?,
        @field:NotEmpty var name: String,
        var email: String,
        @field:ValidPassword var password: String,
        @OneToMany(mappedBy = "user", cascade = [CascadeType.ALL], fetch = FetchType.LAZY, orphanRemoval = true) var phones: MutableList<Phone>?,
        var created: Date? = null,
        var modified: Date? = null,
        var last_login: Date? = null,
        @Column(length = 1000) var token: String? = null,
        var isactive: Boolean? = null

)
