package com.example.nelson.usermanagement.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
data class Phone(@Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Int,
                 var number: Int,
                 var cityCode: Int,
                 var countryCode: Int,
                 @ManyToOne(fetch = FetchType.LAZY)
                 @JoinColumn(name = "user_uuid")
                 @JsonIgnore var user: User?)
