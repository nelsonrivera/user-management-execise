package com.example.nelson.usermanagement.validation

import javax.validation.Constraint
import javax.validation.Payload
import kotlin.reflect.KClass

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.FIELD)
@MustBeDocumented
@Constraint(validatedBy = [ValidPasswordValidator::class])
annotation class ValidPassword(val message: String = "Password must have the format one capital letter, lowercase letters and two numbers",
                               val groups: Array<KClass<*>> = [],
                               val payload: Array<KClass<out Payload>> = [])
