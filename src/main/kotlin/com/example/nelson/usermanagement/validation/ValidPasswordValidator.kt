package com.example.nelson.usermanagement.validation

import java.util.regex.Pattern
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

class ValidPasswordValidator: ConstraintValidator<ValidPassword, String> {

    override fun isValid(password: String?, context: ConstraintValidatorContext?): Boolean {

        return password?.let {

            val inputRegexesList =  listOf(Pattern.compile(".*[A-Z].*"), Pattern.compile(".*[a-z].*"),  Pattern.compile(".*\\d.*\\d.*"))
            inputRegexesList.all { pattern -> pattern!!.matcher(password).matches() }

        } ?: false

    }
}
