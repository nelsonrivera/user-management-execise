package com.example.nelson.usermanagement.exception

class UniqueException: Exception {

    constructor(message: String): super(message)

    constructor(cause: Throwable ): super(cause)
}
