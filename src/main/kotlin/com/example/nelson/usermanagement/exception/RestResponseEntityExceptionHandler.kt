package com.example.nelson.usermanagement.exception

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.JsonNodeFactory
import com.fasterxml.jackson.databind.node.ObjectNode
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@RestControllerAdvice
class RestResponseEntityExceptionHandler: ResponseEntityExceptionHandler() {

    @ExceptionHandler(value = [UniqueException::class])
    protected fun handleUniqueException(ex: UniqueException, request: WebRequest?): ResponseEntity<Any?>? {
        val exceptionPayload: JsonNode? = createExceptionJson(ex.message, HttpStatus.BAD_REQUEST, request)
        return handleExceptionInternal(ex, exceptionPayload, HttpHeaders(), HttpStatus.BAD_REQUEST, request!!)
    }

    @ExceptionHandler(value = [EmptyResultDataAccessException::class])
    protected fun handleEmpty(ex: EmptyResultDataAccessException, request: WebRequest?): ResponseEntity<Any?>? {
        return handleExceptionInternal(ex, ex.message, HttpHeaders(), HttpStatus.NOT_FOUND, request!!)
    }

    override fun handleMethodArgumentNotValid(ex: MethodArgumentNotValidException, headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        var message = ""

        val fieldErrors = ex.bindingResult.fieldErrors
        fieldErrors?.let {

            fieldErrors.forEach() { fieldError -> message = "$message \n ${fieldError.field}: ${fieldError.defaultMessage}" }
        }

        val globalErrors = ex.bindingResult.globalErrors

        globalErrors?.let {

            globalErrors.forEach() { globalError -> message = "$message \n ${globalError.objectName}: ${globalError.defaultMessage}" }
        }

        val exceptionPayload = createExceptionJson(message, HttpStatus.BAD_REQUEST, request)
        return handleExceptionInternal(ex, exceptionPayload, headers!!, HttpStatus.BAD_REQUEST, request!!)
    }

    private fun createExceptionJson(message: String?, httpStatus: HttpStatus, webRequest: WebRequest?): JsonNode? {
        val jsonNode: JsonNode = JsonNodeFactory.instance.objectNode()
        (jsonNode as ObjectNode).put("message", message)
        return jsonNode
    }


}
