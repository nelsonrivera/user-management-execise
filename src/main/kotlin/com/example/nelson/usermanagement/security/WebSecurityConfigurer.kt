package com.example.nelson.usermanagement.security

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.annotation.Order
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.password.PasswordEncoder
import javax.sql.DataSource

@Configuration
@Order(1)
@ConditionalOnProperty(value = ["security.enabled"], matchIfMissing = true)
class WebSecurityConfigurer(val dataSource: DataSource): WebSecurityConfigurerAdapter() {

    @Bean
    override fun userDetailsServiceBean(): UserDetailsService = super.userDetailsServiceBean()

    @Bean
    override fun authenticationManagerBean(): AuthenticationManager = super.authenticationManagerBean()

    override fun configure(http: HttpSecurity?) {
        http!!.csrf().disable().requestMatchers()
                .antMatchers("/login", "/oauth/authorize")
                .and()
                .authorizeRequests()
                .anyRequest()
                .authenticated()
    }

    override fun configure(auth: AuthenticationManagerBuilder?) {

        //using database to authenticate user
        auth!!.jdbcAuthentication().dataSource(dataSource).usersByUsernameQuery("select name, password, isactive"
                + " from user where name=?").authoritiesByUsernameQuery("select user_name, role "
                + "from user_roles where user_name=?")
    }

}
