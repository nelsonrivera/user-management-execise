package com.example.nelson.usermanagement.security

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.oauth2.client.OAuth2ClientContext
import org.springframework.security.oauth2.common.OAuth2AccessToken
import org.springframework.security.oauth2.provider.ClientDetailsService
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.OAuth2Request
import org.springframework.security.oauth2.provider.token.DefaultTokenServices
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter
import org.springframework.stereotype.Component

@Component
@ConditionalOnProperty(value = ["security.enabled"], matchIfMissing = true)
class OAuth2TokenUtils(val clientDetailsService: ClientDetailsService,
                       val accessTokenConverter: JwtAccessTokenConverter,
                       val tokenServices: DefaultTokenServices,
                       @Qualifier("userDetailsServiceBean") val userDetailsService: UserDetailsService,
                       @Qualifier("oauth2ClientContext") val oauth2ClientContext: OAuth2ClientContext) {

    @Value("\${security.oauth2.client.client-id}")
    var clientId: String? = null

    @Value("\${security.oauth2.client.scope}")
    var scope: Set<String>? = null

    fun generateAccessToken(userName: String): OAuth2AccessToken {
        val userDetails = userDetailsService.loadUserByUsername(userName)

        tokenServices.setTokenEnhancer(accessTokenConverter)
        tokenServices.setClientDetailsService(clientDetailsService)
        return tokenServices.createAccessToken(getAuthentication(userDetails))

    }

    fun getAuthentication(userDetails: UserDetails) : OAuth2Authentication {
        val parameters = mapOf("grant_type" to "password", "username" to userDetails.username)

        val request = OAuth2Request(parameters, clientId, null, true, scope, null, null, null, null)
        return OAuth2Authentication(request, UsernamePasswordAuthenticationToken(userDetails, null, userDetails.authorities))
    }

    fun getCurrentToken(): String = oauth2ClientContext.accessToken.value
}
