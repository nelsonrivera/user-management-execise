package com.example.nelson.usermanagement.repositories

import com.example.nelson.usermanagement.domain.User
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UserRepository: PagingAndSortingRepository<User, String> {

    fun findFirstByEmail(email: String?): User?

    fun findFirstByName(name: String?): User?
}
