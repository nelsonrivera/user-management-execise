package com.example.nelson.usermanagement.controller

import com.example.nelson.usermanagement.domain.User
import com.example.nelson.usermanagement.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.bind.annotation.*
import java.security.Principal
import javax.validation.Valid

@RestController
@RequestMapping("/users")
class UserController @Autowired constructor(private val service: UserService){

    @GetMapping("/hello/{name}")
    fun hello(@PathVariable name: String): Unit {
        service.hello(name);
    }

    @GetMapping
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    fun getAll(): ResponseEntity<List<User>> {

        val users = service.getAllUsers()
        return ResponseEntity.ok(users)
    }

    @PostMapping(consumes = ["application/json"])
    fun registerUser(@RequestBody @Valid user: User): ResponseEntity<User> {
        val userSaved: User = service.registerUser(user)

        val multiValueMap: MultiValueMap<String, String> = LinkedMultiValueMap()
        multiValueMap.add("location", "/users/" + userSaved.uuid)

        return ResponseEntity(userSaved, multiValueMap, HttpStatus.CREATED)

    }

    @PostMapping("/login")
    fun login(principal: Principal): ResponseEntity<User> {
        val user = service.login(principal)
        return ResponseEntity.ok(user)
    }

}
